package com.example.task20
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.subjects.PublishSubject

open class RxBus {
    var mInstance:RxBus? = null
    val publisher:PublishSubject<String> = PublishSubject.create()
    fun getInstance():RxBus{
        if(mInstance == null){
            mInstance = RxBus()
        }
        return mInstance!!
    }

    fun publish(event:String){
        publisher.onNext(event)
    }



    fun listen():Observable<String>{
        return publisher
    }


}
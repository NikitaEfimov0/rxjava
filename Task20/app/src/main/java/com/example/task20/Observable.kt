package com.example.task20

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.task18.MyDataItem
import com.example.task20.databinding.FragmentObservableBinding
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.IOException
import java.lang.reflect.Type
import java.nio.charset.Charset
import kotlin.random.Random

class Observable : Fragment() {
    val BASE_URL = "https://jsonplaceholder.typicode.com/"
    lateinit var binding: FragmentObservableBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentObservableBinding.inflate(inflater)

        binding.fromAPI.setOnClickListener {
            try {
                getMyData()
                binding.ifsuccess.text = "Successfully downloaded"
            }catch (e:IOException){
                binding.ifsuccess.text = "There is some error with downloading data :("
            }

        }

        binding.fromFile.setOnClickListener {
            try {
                getDataFromJSONFile()
                binding.ifsuccess.text = "Successfully parsed"
            }catch (e:IOException){
                binding.ifsuccess.text = "There is some error with parsing data :("
            }
        }


        return binding.root
    }


    private fun getMyData() {
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(APIInterface::class.java)

        val retrofitData = retrofitBuilder.getData()

        retrofitData.enqueue(object : Callback<List<MyDataItem>?> {
            override fun onResponse(
                call: Call<List<MyDataItem>?>?,
                response: Response<List<MyDataItem>?>?
            ) {
                val responseBody = response?.body()!!


                CurrentBus.bus.publish(getRandomString(responseBody))

            }

            override fun onFailure(call: Call<List<MyDataItem>?>?, t: Throwable?) {

            }
        })
    }


    private fun getDataFromJSONFile(){
        try {
            val file = File(context?.filesDir, "src/main/res/raw/data.json")
            val fileReader = FileReader(file)
            val bufferedReader = BufferedReader(fileReader)
            val stringBuilder = StringBuilder()
            var line: String = bufferedReader.readLine()
            while (line != null) {
                stringBuilder.append(line).append("\n")
                line = bufferedReader.readLine()
            }
            bufferedReader.close()
            val responce = stringBuilder.toString()

            val listType: Type = object: TypeToken<List<MyDataItem>>(){}.type
            val listOfMessage:List<MyDataItem> = Gson().fromJson(responce, listType)


            CurrentBus.bus.publish(getRandomString(listOfMessage))


        }catch (e:IOException){
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()
            throw e
        }

    }


    fun getRandomString(mList:List<MyDataItem>):String{
        val i:Int = Random.nextInt(0, mList.size-1)
        return mList[i].body

    }

    companion object {

        @JvmStatic
        fun newInstance() = Observable()
    }


}
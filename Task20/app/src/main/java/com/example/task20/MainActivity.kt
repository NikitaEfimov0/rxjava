package com.example.task20

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.example.task20.databinding.ActivityMainBinding
import java.lang.NullPointerException

class MainActivity : AppCompatActivity() {
    var currentId:Int = R.id.launch
    lateinit var binding:ActivityMainBinding
    var fragmManager:FragmentManager = supportFragmentManager
    var fr1:Observer? = null
    var fr2:Observable?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(this.layoutInflater)
        setContentView(binding.root)
        fr1 = Observer()
        fragmManager.beginTransaction().add(R.id.fragmenShow, fr1!!).commit()
        binding.navMenu.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.launch ->{
                        if (fr1 != null) {
                            fragmManager.beginTransaction().hide(fr2!!).show(fr1!!).commit()
                            currentId = it.itemId
                        }


//

                }
                R.id.download-> {
                        if(fr2!=null){
                        fragmManager.beginTransaction().hide(fr1!!).show(fr2!!).commit()
                        currentId = it.itemId
                            }
                    if(fr2 == null){
                        fr2 = Observable()
                        fragmManager.beginTransaction().add(R.id.fragmenShow, fr2!!).hide(fr1!!).commit()
                    }
                }
            }
            true
        }

    }
}
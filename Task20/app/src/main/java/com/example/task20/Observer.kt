package com.example.task20

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.task20.databinding.FragmentObserverBinding
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.internal.operators.observable.ObserverResourceWrapper

class Observer : Fragment() {

    lateinit var binding:FragmentObserverBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentObserverBinding.inflate(inflater)


        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.button.setOnClickListener {
            CurrentBus.bus.listen().subscribe(createObs());
        }
    }


    private fun createObs():Observer<String>{
        val observer:Observer<String> = object: Observer<String>{
            override fun onSubscribe(d: Disposable) {
                binding.textView.text = "Subscribed!"
            }

            override fun onNext(t: String) {
                binding.textView.text = t




            }

            override fun onError(e: Throwable) {
                binding.textView.text = "Error!"
            }

            override fun onComplete() {
                binding.textView.text = "Complete!"
            }

        }
        return observer
    }

    companion object {

        @JvmStatic
        fun newInstance() = Observer()
    }


}